from discord_webhook import DiscordWebhook, DiscordEmbed
import time, os, shutil, json
import settings

#----------------------------------#
# Build embedded Discord messages. #
#----------------------------------#
def discord_embed_builderv2(name, user_name, profile_img, tweet_text, tweet_id, tweet_timestamp):
    discord_embed = DiscordEmbed()
    discord_embed.set_author(name=f'{name} ({user_name})', icon_url=profile_img, url=f'https://twitter.com/{user_name}/status/{tweet_id}')
    discord_embed.set_color(color='1da1f2')    
    discord_embed.set_timestamp(tweet_timestamp)
    discord_embed.set_footer(text='Twitter')
    discord_embed.set_description(description=tweet_text)
    return discord_embed_sendv2(discord_embed, settings.twitter_accounts[user_name]['webhook_url'])

#-------------------------------------------#
# Send embedded Discord message to Discord. #
#-------------------------------------------#
def discord_embed_sendv2(discord_embed, discord_url):
    discord_webhook = DiscordWebhook(url=discord_url, username='CA Bot', avatar_url='https://img-9gag-fun.9cache.com/photo/a6OPpNb_460s.jpg')
    discord_webhook.add_embed(discord_embed)
    return discord_webhook.execute()

#-----------------------------------#
# Send tweet media/urls to Discord. #
#-----------------------------------#
def discord_media_sendv2(media_url, discord_url):
    discord_webhook = DiscordWebhook(url=discord_url, username='CA Bot', avatar_url='https://img-9gag-fun.9cache.com/photo/a6OPpNb_460s.jpg', content=media_url)
    return discord_webhook.execute()

#-------------------------------------------------#
# Primary function to send Tweet data to Discord. #
#-------------------------------------------------#
def discord_core(tweet_data):
    # Set rate limiting initial variables
    rate_limit_remaining = 1
    rate_limit_reset_after = 1

    # Loop through all tweets and media files in tweet_data dictionary
    for tweet_object in tweet_data['tweets']:

        # Detect rate limiting and sleep for as long as Discord returned for rate limiting time
        if rate_limit_remaining == 0:
            print(f'Sleeping {rate_limit_remaining}')

        # Execute sending tweet to embedded Discord message, store Discord response/headers for rate limiting tracking
        response = discord_embed_builderv2(tweet_data['name'], tweet_data['user_name'], tweet_data['profile_img'], tweet_object['tweet_text'], tweet_object['tweet_id'], tweet_object['tweet_timestamp'])
        rate_limit_remaining = int(response.headers._store['x-ratelimit-remaining'][1])
        rate_limit_reset_after = int(response.headers._store['x-ratelimit-reset-after'][1])
        print(f'Rate Limit Remaining: {rate_limit_remaining} / Rate Limit Reset After: {rate_limit_reset_after}')

        # Loop through any associated tweet media/links
        if tweet_object['tweet_media'] != []:
            for media_url in tweet_object['tweet_media']:

                # Detect rate limiting and sleep for as long as Discord returned for rate limiting time
                if rate_limit_remaining == 0:
                    time.sleep(rate_limit_reset_after)

                # Execute sending media to Discord
                response = discord_media_sendv2(media_url, settings.twitter_accounts[tweet_data['user_name']]['webhook_url'])
                rate_limit_remaining = int(response.headers._store['x-ratelimit-remaining'][1])
                rate_limit_reset_after = int(response.headers._store['x-ratelimit-reset-after'][1])
                print(f'Rate Limit Remaining: {rate_limit_remaining} / Rate Limit Reset After: {rate_limit_reset_after}')

if __name__ == '__main__':
    print('This script is not meant to be run on it\'s own.')
    quit()
