import configparser
import json

def init():
    global config
    with open('conf/settings.json', 'r') as settings_file:
        config = json.load(settings_file)

    global twitter_accounts
    global twitter_accounts_modified
    with open('conf/accounts.json', 'r') as accounts_file:
        twitter_accounts = json.load(accounts_file)
        twitter_accounts_modified = False

    global twitter_state
    with open('conf/twitter_state.json', 'r') as state_file:
        twitter_state = json.load(state_file)

    global discord_notification
    with open('conf/notification_tag.json') as notification_file:
        discord_notification = json.load(notification_file)

    global newsletter_state
    with open('conf/newsletter.json', 'r') as file:
        newsletter_state = json.load(file)
    
def write_twitter_state():
    with open('conf/twitter_state.json', 'w') as file:
        file.write(json.dumps(twitter_state, indent=4))

def write_twitter_accounts():
    if twitter_accounts_modified:
        with open('conf/accounts.json', 'w') as file:
            file.write(json.dumps(twitter_accounts, indent=4))

def write_newsletter_state():
    with open('conf/newsletter.json', 'w') as file:
        file.write(json.dumps(newsletter_state, indent=4))