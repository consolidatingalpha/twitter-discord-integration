import tweepy as tw
import settings, discord
import argparse, os

#----------------------------------------------------------------------#
# Initializes Twitter client, loading configuration from settings file #
#----------------------------------------------------------------------#
def initialize_client():
    client = tw.Client(consumer_key=settings.config['twitter_creds']['consumer_key'],
        consumer_secret=settings.config['twitter_creds']['consumer_secret'],
        access_token=settings.config['twitter_creds']['access_token'],
        access_token_secret=settings.config['twitter_creds']['access_token_secret'])
    return client

#---------------------------------------------------------------------#
# Gets all tweets for a user account. If no latest tweets tracked,    #
#   gets latest 10 tweets from user account based on default settings #
#---------------------------------------------------------------------#
def get_user_tweets(client, user_id, since_tweet):
    if since_tweet == 'None':
        tweets = client.get_users_tweets(id=user_id, user_auth=True, exclude=['replies', 'retweets'], expansions='attachments.media_keys', media_fields=['url', 'variants'], tweet_fields=['created_at', 'entities'])
    else:
        tweets = client.get_users_tweets(id=user_id, since_id=since_tweet, user_auth=True, exclude=['replies', 'retweets'], expansions='attachments.media_keys', media_fields=['url', 'variants'], tweet_fields=['created_at', 'entities'])
    return tweets

#--------------------------------------------------------------------#
# Extracts all media from a tweet. Includes embedded images, videos, #
#   and external links. Returns a list of media links.               #
#--------------------------------------------------------------------#
def extract_media(tweet, media):

    tweet_media = list()

    # Extracts embedded images and videos, adds to media list object
    try:
        if 'media' in tweet.includes:
            for media in tweet.includes['media']:
                if media.url != None:
                    tweet_media.append(media.url)
                    continue
                tweet_media.append(media.data['variants'][0]['url'])
    except:
        print()

    # Extracts external urls excluding Twitter links
    try:
        if 'urls' in tweet.data['entities']:
            for url in tweet.data['entities']['urls']:
                expanded_url = url['expanded_url']
                if 'twitter.com' not in expanded_url:
                    tweet_media.append(expanded_url)
    except:
        print()

    try:
        if tweet.attachments != None:
            for media_keys in tweet.attachments['media_keys']:
                tweet_media.append(media[media_keys])
    except:
        print()        

    return tweet_media

#---------------------------------------------------#
# Primary function to begin ingesting Twitter data. #
#---------------------------------------------------#
def twitter_core():

    # Initializes Twitter API client
    client = initialize_client()

    # Loops through all accounts in Twitter accounts file
    for account in settings.twitter_accounts:
        
        # Dictionary to store all tweets and additional metadata to pass to Discord/other functions
        all_data = dict()

        # List of dictionaries storing all tweets per account to be added to all_data
        all_tweets = list() 

        # Performs checks if user metadata already stored to reduce API calls
        if ('user_name' not in settings.twitter_accounts[account] or 
            'profile_img' not in settings.twitter_accounts[account] or
            'user_id' not in settings.twitter_accounts[account] or
            'name' not in settings.twitter_accounts[account]):
            user = client.get_user(username=account, user_auth=True, user_fields='profile_image_url')
            settings.twitter_accounts[account]['user_name'] = user.data.username
            settings.twitter_accounts[account]['profile_img'] = user.data.profile_image_url
            settings.twitter_accounts[account]['user_id'] = user.data.id
            settings.twitter_accounts[account]['name'] = user.data.name
            settings.twitter_accounts_modified = True
            settings.write_twitter_accounts()
        
        user = client.get_user(username=account, user_auth=True, user_fields='profile_image_url')

        # Initial Twitter metadata for user account to pass through to other functions
        all_data['user_name'] = settings.twitter_accounts[account]['user_name']
        all_data['name'] = settings.twitter_accounts[account]['name']
        all_data['profile_img'] = settings.twitter_accounts[account]['profile_img']

        if account not in settings.twitter_state:
            tweet_id = dict()
            tweet_id['tweet_id'] = 'None'
            settings.twitter_state[account] = tweet_id

        # Gets all user Tweets, stores into a Tweet list, reverses order to process oldest first
        tweets = get_user_tweets(client, settings.twitter_accounts[account]['user_id'], settings.twitter_state[account]['tweet_id'])
        if tweets.data == None:
            continue
        tweets.data.reverse()
        tweet_media_keys = dict()

        # Modify media list to dictionary with media_key as dictionary key, speeds up lookups
        if tweets.includes != {}:
            if tweets.includes['media'] != None:
                for media in tweets.includes['media']:
                    tweet_media_keys[media['media_key']] = media['url']

        # Iterates through retrieved tweets to extract Tweet text and linked media, stores in all_tweets
        for tweet in tweets.data:
            tweet_data = dict()
            tweet_data['tweet_id'] = tweet.id
            tweet_data['tweet_timestamp'] = tweet.created_at.timestamp()

            tweet_data['tweet_text'] = tweet.text
            tweet_data['tweet_text'] = tweet_data['tweet_text'].replace('&amp;', '&')
            tweet_data['tweet_text'] = tweet_data['tweet_text'].replace('&lt;', '<')
            tweet_data['tweet_text'] = tweet_data['tweet_text'].replace('&gt;', '>')

            tweet_data['tweet_media'] = extract_media(tweet, tweet_media_keys)
            all_tweets.append(tweet_data)

            settings.twitter_state[account]['tweet_id'] = tweet.id
        
        all_data['tweets'] = all_tweets

        # Pass all tweet data for account to Discord function
        discord.discord_core(all_data)

    settings.write_twitter_state()

if __name__ == '__main__':
    # Gets execution directory from argument passed to script
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', help='Set directory')

    args = parser.parse_args()
    if args.directory != None:
        os.chdir(args.directory)

    # Loads settings
    settings.init()

    # Initiates twitter_core function
    twitter_core()